package imit;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class UrlConnectionPage {

    public static void urlConnection(String url) throws IOException {
        URLConnection connection = new URL(url).openConnection();
        InputStream is = connection.getInputStream();
        InputStreamReader reader = new InputStreamReader(is);
        char[] buffer = new char[1024];
        int rc;
        StringBuilder sb = new StringBuilder();
        while ((rc = reader.read(buffer)) != -1)
            sb.append(buffer, 0, rc);
        System.out.println(connection.getHeaderFields());
        FileWriter fileWriter = new FileWriter("page.html");
        fileWriter.write(sb.toString());
        fileWriter.close();
        reader.close();
    }

    public static void main(String[] args) throws Exception {

        System.out.println("Enter the URL:\n");
        Scanner scanner = new Scanner(System.in);
        String url = scanner.nextLine();
        urlConnection(url);

    }
}
