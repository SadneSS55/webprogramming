package imit;


import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class socketPage {

    public static void main(String[] args) throws IOException {
        System.out.println("Enter the URL:");
        Scanner scanner = new Scanner(System.in);
        String url = scanner.nextLine();
        try (Socket socket = new Socket(url, 80)) {
            int tmp;
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
            printWriter.println("GET / HTTP/1.1");
            printWriter.println("");
            printWriter.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            char[] buffer = new char[8192];
            String tmpstr;
            int rc;
            StringBuilder sb = new StringBuilder();
            while (!(tmpstr = reader.readLine()).equals(""))
            {
                System.out.println(tmpstr);
            }
            while ((rc = reader.read(buffer)) != -1) {
                sb.append(buffer, 0, rc);
                if (sb.charAt(sb.length() - 1) == 10) {
                    break;
                }
            }
            reader.close();
            FileWriter fileWriter = new FileWriter("page.html");
            fileWriter.write(sb.toString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
