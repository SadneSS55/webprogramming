const boardSize = 8
const board = Array.from(Array(boardSize), () => Array(boardSize))
let boardView
let situation = new Map()
let promptMode = false
let whoseTurn = 'w'

const cellState = {
    DEFAULT: 0,
    PROMPT: 1,
    CAN_BE_FILLED: 2,
    MUST_BE_FILLED: 3
}
const cellStateClass = {
    [cellState.PROMPT]: 'prompt',
    [cellState.CAN_BE_FILLED]: 'can-be-filled',
    [cellState.MUST_BE_FILLED]: 'must-be-filled'
}
const checkerType = {
    BLACK: -1,
    BLACK_KING: -2,
    WHITE: 1,
    WHITE_KING: 2
}
const CHECKER_PIC = {
    [checkerType.BLACK]: '../img/black.svg',
    [checkerType.BLACK_KING]: '../img/blackKing.svg',
    [checkerType.WHITE]: '../img/white.svg',
    [checkerType.WHITE_KING]: '../img/whiteKing.svg'
}
const CHECKER_COLOR = {
    [checkerType.BLACK]: 'b',
    [checkerType.BLACK_KING]: 'b',
    [checkerType.WHITE]: 'w',
    [checkerType.WHITE_KING]: 'w'
}

const statusStr = document.getElementById('status')
const startButton = document.getElementById('start')
const example1button = document.getElementById('example1')
const finishTurnButton = document.getElementById('finish-turn')


const isPlayCell = (row, col) => (row + col) % 2 === 0


const hasChecker = (row, col) => board[row][col]?.checker != null


const renderChecker = (row, col) => {
    const checker = board[row][col].checker

    boardView[row][col].innerHTML = checker == null ? '' : '<img src="' + CHECKER_PIC[checker.type] + '" alt="">'
}
const renderCell = (row, col) => {
    if (!isPlayCell(row, col))
        return

    const state = board[row][col].state

    if (state === cellState.DEFAULT)
        boardView[row][col].removeAttribute('class')
    else
        boardView[row][col].className = cellStateClass[state]

    renderChecker(row, col)
}


const renderBoard = () => {
    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            renderCell(row, col)
}


const turn = () => {
    if (whoseTurn === 'w') {
        whoseTurn = 'b'
        statusStr.innerText = 'Blacks turn'
    } else {
        whoseTurn = 'w'
        statusStr.innerText = 'Whites turn'
    }

    calculateSituation()
}


const isWhite = (row, col) => {
    const type = board[row][col]?.checker?.type

    return type === checkerType.WHITE || type === checkerType.WHITE_KING
}


const isBlack = (row, col) => {
    const type = board[row][col]?.checker?.type

    return type === checkerType.BLACK || type === checkerType.BLACK_KING
}


const isTurnOf = (row, col) => {
    if (!hasChecker(row, col))
        return false

    const type = board[row][col].checker.type

    return (whoseTurn === 'w' && (type === checkerType.WHITE || type === checkerType.WHITE_KING)) ||
        (whoseTurn === 'b' && (type === checkerType.BLACK || type === checkerType.BLACK_KING))
}


const addToSituation = (rowFrom, colFrom, rowTo, colTo, state) => {
    const cellFrom = board[rowFrom][colFrom]
    let cellsTo = situation.get(cellFrom)
    const newCell = {row: rowTo, col: colTo, state: state}

    if (cellsTo == null) {
        cellsTo = [newCell]
        situation.set(cellFrom, cellsTo)
    } else
        cellsTo.push(newCell)
}


const areFoes = (row1, col1, row2, col2) => {
    const color1 = CHECKER_COLOR[board[row1][col1]?.checker?.type]
    const color2 = CHECKER_COLOR[board[row2][col2]?.checker?.type]

    return color1 != null && color2 != null && color1 !== color2
}


const iterator = (row, col, rowDir, colDir) => {
    return {
        next: () => {
            row += rowDir
            col += colDir

            return (row > -1 && row < boardSize && col > -1 && col < boardSize) ?
                {value: {row: row, col: col}, done: false} :
                {done: true}
        }
    }
}


const calculateSituation = () => {
    situation.clear()

    let foundMustBeFilled = false

    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++) {
            if (!isTurnOf(row, col))
                continue

            const type = board[row][col].checker.type

            if (type === checkerType.WHITE_KING || type === checkerType.BLACK_KING) {
                let it;
                for (it of [iterator(row, col, 1, -1), iterator(row, col, 1, 1), iterator(row, col, -1, 1), iterator(row, col, -1, -1)]) {
                    let res = it.next()
                    let foundFoe = false

                    while (!res.done) {
                        let {row: rowTo, col: colTo} = res.value

                        if (!hasChecker(rowTo, colTo)) {
                            if (foundFoe) {
                                addToSituation(row, col, rowTo, colTo, cellState.MUST_BE_FILLED)
                                foundMustBeFilled = true
                                break
                            } else if (!foundMustBeFilled)
                                addToSituation(row, col, rowTo, colTo, cellState.CAN_BE_FILLED)
                        } else if (areFoes(row, col, rowTo, colTo))
                            foundFoe = true

                        else
                            break

                        res = it.next()
                    }
                }
            } else {
                if (row < boardSize - 2) {
                    if (col > 1 && areFoes(row, col, row + 1, col - 1) && !hasChecker(row + 2, col - 2)) {
                        addToSituation(row, col, row + 2, col - 2, cellState.MUST_BE_FILLED)
                        foundMustBeFilled = true
                    }

                    if (col < boardSize - 2 && areFoes(row, col, row + 1, col + 1) && !hasChecker(row + 2, col + 2)) {
                        addToSituation(row, col, row + 2, col + 2, cellState.MUST_BE_FILLED)
                        foundMustBeFilled = true
                    }
                }

                if (row > 1) {
                    if (col > 1 && areFoes(row, col, row - 1, col - 1) && !hasChecker(row - 2, col - 2)) {
                        addToSituation(row, col, row - 2, col - 2, cellState.MUST_BE_FILLED)
                        foundMustBeFilled = true
                    }

                    if (col < boardSize - 2 && areFoes(row, col, row - 1, col + 1) && !hasChecker(row - 2, col + 2)) {
                        addToSituation(row, col, row - 2, col + 2, cellState.MUST_BE_FILLED)
                        foundMustBeFilled = true
                    }
                }

                if (!foundMustBeFilled) {
                    if (isWhite(row, col) && row < boardSize - 1) {
                        if (col > 0 && !hasChecker(row + 1, col - 1))
                            addToSituation(row, col, row + 1, col - 1, cellState.CAN_BE_FILLED)

                        if (col < boardSize - 1 && !hasChecker(row + 1, col + 1))
                            addToSituation(row, col, row + 1, col + 1, cellState.CAN_BE_FILLED)
                    } else if (isBlack(row, col) && row > 0) {
                        if (col > 0 && !hasChecker(row - 1, col - 1))
                            addToSituation(row, col, row - 1, col - 1, cellState.CAN_BE_FILLED)

                        if (col < boardSize - 1 && !hasChecker(row - 1, col + 1))
                            addToSituation(row, col, row - 1, col + 1, cellState.CAN_BE_FILLED)
                    }
                }
            }
        }

    if (foundMustBeFilled)
        for (let entry of situation) {
            const [cellFrom, cellsTo] = entry
            const filteredCellsTo = cellsTo.filter(cellTo => cellTo.state === cellState.MUST_BE_FILLED)

            if (filteredCellsTo.length === 0)
                situation.delete(cellFrom)
            else
                situation.set(cellFrom, filteredCellsTo)
        }
}


const togglePromptMode = (row, col) => {
    if (!isTurnOf(row, col))
        return []

    const state = board[row][col].state
    const cellsTo = situation.get(board[row][col]) || []

    if (promptMode && state === cellState.PROMPT) {
        promptMode = false
        board[row][col].state = cellState.DEFAULT

        let cell;
        for (cell of cellsTo)
            board[cell.row][cell.col].state = cellState.DEFAULT
    } else if (!promptMode && state === cellState.DEFAULT) {
        promptMode = true
        board[row][col].state = cellState.PROMPT
        let cell
        for (cell of cellsTo)
            board[cell.row][cell.col].state = cell.state
    }

    return cellsTo.map(cell => ({row: cell.row, col: cell.col}))
}


const cellOnClick = (row, col) => {
    const changedCells = togglePromptMode(row, col)

    renderCell(row, col)
    changedCells.forEach(cell => renderCell(cell.row, cell.col))
}


const startArrangement = () => {
    for (let row = 0; row < 3; row++)
        for (let col = 0; col < boardSize; col++)
            if (isPlayCell(row, col))
                board[row][col].checker = {type: checkerType.WHITE}

    for (let row = 3; row < 5; row++)
        for (let col = 0; col < boardSize; col++)
            if (isPlayCell(row, col))
                board[row][col].checker = null

    for (let row = 5; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            if (isPlayCell(row, col))
                board[row][col].checker = {type: checkerType.BLACK}
}


const example1Arrangement = () => {
    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            if (isPlayCell(row, col))
                board[row][col].checker = null

    board[3][5].checker = {type: checkerType.WHITE}
    board[3][7].checker = {type: checkerType.WHITE}

    board[7][1].checker = {type: checkerType.BLACK}
    board[0][2].checker = {type: checkerType.BLACK_KING}
    board[4][2].checker = {type: checkerType.BLACK}
    board[6][2].checker = {type: checkerType.BLACK}
    board[6][4].checker = {type: checkerType.BLACK}
    board[5][7].checker = {type: checkerType.BLACK}
}


const init = () => {
    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            if (isPlayCell(row, col))
                board[row][col] = {state: cellState.DEFAULT}

    let col = 0

    boardView = Array.from(document.querySelectorAll('.board tr td'))
        .reduce((arr, cell, index) => {
            const row = boardSize - 1 - Math.floor(index / boardSize)

            arr[row] = arr[row] || []
            arr[row].push(isPlayCell(row, col) ? cell : null)

            col = (++col) % boardSize

            return arr
        }, Array(boardSize))

    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            boardView[row][col]?.addEventListener('click', () => cellOnClick(row, col))

    startButton.addEventListener('click', () => {
        startArrangement()
        calculateSituation()
        renderBoard()
    })
    example1button.addEventListener('click', () => {
        example1Arrangement()
        calculateSituation()
        renderBoard()
    })
    finishTurnButton.addEventListener('click', () => turn())
}


init()