const boardSize = 8
const board = Array.from(Array(boardSize), () => Array(boardSize))
let boardMap
let boardView
let situation = new Map()
let inPromptMode = null
let moveList = []
let becomeKing = false
let killed = []
let whoseTurn = 'w'
let buttonsVisible = false
let whiteCounter = 0
let blackCounter = 0

const cellState = {
    default: 0,
    prompt: 1,
    canBeFilled: 2,
    mustBeFilled: 3,
    killed: 4
}
const cellStateClass = {
    [cellState.prompt]: 'prompt',
    [cellState.canBeFilled]: 'can-be-filled',
    [cellState.mustBeFilled]: 'must-be-filled',
    [cellState.killed]: 'killed'
}
const checkerType = {
    black: -1,
    blackKing: -2,
    white: 1,
    whiteKing: 2
}
const checkerPic = {
    [checkerType.black]: '../img/black.svg',
    [checkerType.blackKing]: '../img/blackKing.svg',
    [checkerType.white]: '../img/white.svg',
    [checkerType.whiteKing]: '../img/whiteKing.svg'
}
const checkerColor = {
    [checkerType.black]: 'b',
    [checkerType.blackKing]: 'b',
    [checkerType.white]: 'w',
    [checkerType.whiteKing]: 'w'
}

const statusStr = document.getElementById('status')
const start = document.getElementById('start')
const example1 = document.getElementById('example1')
const cancel = document.getElementById('cancel-turn')
const finish = document.getElementById('finish-turn')
const moveListView = document.getElementById('move-list')


const startArrangement = () => {
    boardMap =
        [[0, -1, 0, -1, 0, -1, 0, -1],
            [-1, 0, -1, 0, -1, 0, -1, 0],
            [0, -1, 0, -1, 0, -1, 0, -1],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0]];

    boardMap.reverse();

    arrange()
}


const example1Arrangement = () => {
    boardMap =
        [[0, -1, 0, 0, 0, 0, 0, 0],
            [0, 0, -1, 0, -1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, -1],
            [0, 0, -1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1, 0, 1],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, -2, 0, 0, 0, 0, 0]];

    boardMap.reverse();

    arrange()

}


const arrange = () => {

    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            if (isPlayCell(row, col))
                clearChecker(row, col)

    for (let row = 0; row < boardSize; row++) {
        for (let col = 0; col < boardSize; col++) {
            if (!isPlayCell(row, col)) {
                continue;
            }
            if (boardMap[row][col] === -1) {
                place(checkerType.black, row, col)
            } else if (boardMap[row][col] === 1) {
                place(checkerType.white, row, col)
            } else if (boardMap[row][col] === -2) {
                place(checkerType.blackKing, row, col)
            } else if (boardMap[row][col] === 2) {
                place(checkerType.whiteKing, row, col)
            }
        }
    }
}


const isPlayCell = (row, col) => (row + col) % 2 === 0


const hasChecker = (row, col) => board[row][col]?.checker != null


const renderChecker = (row, col) => {
    const checker = board[row][col].checker

    boardView[row][col].innerHTML = checker == null ? '' : '<img src="' + checkerPic[checker.type] + '" alt="">'
}


const place = (type, row, col) => {
    const cell = board[row][col]

    cell.checker = {type: type, cell: cell}
}


const clearChecker = (row, col) => {
    board[row][col].checker = null
}


const move = (rowFrom, colFrom, rowTo, colTo) => {
    const type = board[rowFrom][colFrom].checker.type

    clearChecker(rowFrom, colFrom)
    place(type, rowTo, colTo)
}


const renderCell = (row, col) => {
    if (!isPlayCell(row, col))
        return

    const state = board[row][col].state

    if (state === cellState.default)
        boardView[row][col].removeAttribute('class')
    else
        boardView[row][col].className = cellStateClass[state]

    renderChecker(row, col)
}


const renderBoard = () => {
    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            renderCell(row, col)
}


const toggleTurn = () => {
    whoseTurn = whoseTurn === 'w' ? 'b' : 'w'

    calculateSituation()
}


const renderStatus = () => {
    if (whiteCounter === 0 || blackCounter === 0)
        statusStr.innerText = (whoseTurn === 'w' ? 'Blacks' : 'Whites') + ' wins!'

    else
        statusStr.innerText = (whoseTurn === 'w' ? 'Whites' : 'Blacks') + ' turn'
}


const renderButtons = () => {
    if (buttonsVisible) {
        cancel.classList.remove('hidden')
        finish.classList.remove('hidden')
    } else {
        cancel.classList.add('hidden')
        finish.classList.add('hidden')
    }
}


const cellToString = cell => {
    const letters = 'ABCDEFGH'

    return letters[cell.col] + (cell.row + 1)
}


const renderMoveList = () => {
    const delimeter = killed.length === 0 ? '-' : ':'

    if (whoseTurn === 'w') {
        const turnView = document.createElement('li')
        turnView.appendChild(document.createTextNode(moveList.map(cell => cellToString(cell)).join(delimeter)))
        moveListView.appendChild(turnView)
    } else {
        const moveViews = moveListView.getElementsByTagName('li')
        const turnView = moveViews[moveViews.length - 1]
        turnView.textContent += ' ' + moveList.map(cell => cellToString(cell)).join(delimeter)
    }

    moveListView.scrollTop = moveListView.scrollHeight
}


const isWhite = (row, col) => {
    const type = board[row][col]?.checker?.type

    return type === checkerType.white || type === checkerType.whiteKing
}


const isBlack = (row, col) => {
    const type = board[row][col]?.checker?.type

    return type === checkerType.black || type === checkerType.blackKing
}


const isTurnOf = (row, col) => {
    if (!hasChecker(row, col))
        return false

    const type = board[row][col].checker.type

    return (whoseTurn === 'w' && (type === checkerType.white || type === checkerType.whiteKing)) ||
        (whoseTurn === 'b' && (type === checkerType.black || type === checkerType.blackKing))
}


const addToSituation = (cell, dest, state, foe) => {
    let dests = situation.get(cell)
    const newDest = {dest: dest, state: state, foe: foe}

    if (dests === undefined)
        situation.set(cell, [newDest])

    else
        dests.push(newDest)
}


const areFoes = (row1, col1, row2, col2) => {
    const color1 = checkerColor[board[row1][col1]?.checker?.type]
    const color2 = checkerColor[board[row2][col2]?.checker?.type]

    return color1 != null && color2 != null && color1 !== color2
}


const iterator = (row, col, rowDir, colDir) => {
    return {
        next: () => {
            row += rowDir
            col += colDir

            return (row > -1 && row < boardSize && col > -1 && col < boardSize) ?
                {value: {row: row, col: col}, done: false} :
                {done: true}
        }
    }
}


const calculateSituation = () => {
    situation.clear()

    let foundMustBeFilled = false

    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++) {
            if (!isTurnOf(row, col))
                continue

            const type = board[row][col].checker.type

            if (type === checkerType.whiteKing || type === checkerType.blackKing) {
                let it
                for (it of [iterator(row, col, 1, -1), iterator(row, col, 1, 1), iterator(row, col, -1, 1), iterator(row, col, -1, -1)]) {
                    let res = it.next()
                    let foe = null

                    while (!res.done) {
                        let {row: rowTo, col: colTo} = res.value

                        if (!hasChecker(rowTo, colTo)) {
                            if (foe !== null) {
                                addToSituation(board[row][col], board[rowTo][colTo], cellState.mustBeFilled, foe)
                                foundMustBeFilled = true
                            } else if (!foundMustBeFilled)
                                addToSituation(board[row][col], board[rowTo][colTo], cellState.canBeFilled)
                        } else if (foe === null && areFoes(row, col, rowTo, colTo))
                            foe = board[rowTo][colTo]

                        else
                            break

                        res = it.next()
                    }
                }
            } else {
                if (row < boardSize - 2) {
                    if (col > 1 && areFoes(row, col, row + 1, col - 1) && !hasChecker(row + 2, col - 2)) {
                        addToSituation(board[row][col], board[row + 2][col - 2], cellState.mustBeFilled, board[row + 1][col - 1])
                        foundMustBeFilled = true
                    }

                    if (col < boardSize - 2 && areFoes(row, col, row + 1, col + 1) && !hasChecker(row + 2, col + 2)) {
                        addToSituation(board[row][col], board[row + 2][col + 2], cellState.mustBeFilled, board[row + 1][col + 1])
                        foundMustBeFilled = true
                    }
                }

                if (row > 1) {
                    if (col > 1 && areFoes(row, col, row - 1, col - 1) && !hasChecker(row - 2, col - 2)) {
                        addToSituation(board[row][col], board[row - 2][col - 2], cellState.mustBeFilled, board[row - 1][col - 1])
                        foundMustBeFilled = true
                    }

                    if (col < boardSize - 2 && areFoes(row, col, row - 1, col + 1) && !hasChecker(row - 2, col + 2)) {
                        addToSituation(board[row][col], board[row - 2][col + 2], cellState.mustBeFilled, board[row - 1][col + 1])
                        foundMustBeFilled = true
                    }
                }

                if (!foundMustBeFilled) {
                    if (isWhite(row, col) && row < boardSize - 1) {
                        if (col > 0 && !hasChecker(row + 1, col - 1))
                            addToSituation(board[row][col], board[row + 1][col - 1], cellState.canBeFilled)

                        if (col < boardSize - 1 && !hasChecker(row + 1, col + 1))
                            addToSituation(board[row][col], board[row + 1][col + 1], cellState.canBeFilled)
                    } else if (isBlack(row, col) && row > 0) {
                        if (col > 0 && !hasChecker(row - 1, col - 1))
                            addToSituation(board[row][col], board[row - 1][col - 1], cellState.canBeFilled)

                        if (col < boardSize - 1 && !hasChecker(row - 1, col + 1))
                            addToSituation(board[row][col], board[row - 1][col + 1], cellState.canBeFilled)
                    }
                }
            }
        }

    if (foundMustBeFilled)
        for (let entry of situation) {
            const [cellFrom, cellsTo] = entry
            const filteredCellsTo = cellsTo.filter(cellTo => cellTo.state === cellState.mustBeFilled && cellTo.foe.state !== cellState.killed)

            if (filteredCellsTo.length === 0)
                situation.delete(cellFrom)
            else
                situation.set(cellFrom, filteredCellsTo)
        }
}


const togglePromptMode = cell => {
    if (!isTurnOf(cell.row, cell.col))
        return []

    const dests = situation.get(cell) || []

    if (inPromptMode === cell) {
        inPromptMode = null
        cell.state = cellState.default

        let dest
        for (dest of dests)
            dest.dest.state = cellState.default
    } else if (inPromptMode === null && (moveList.length === 0 || (killed.length !== 0 && dests.length !== 0))) {
        inPromptMode = cell
        cell.state = cellState.prompt

        let dest
        for (dest of dests)
            dest.dest.state = dest.state
    }

    let changedCells = dests.map(dest => dest.dest)
    changedCells.push(cell)

    return changedCells
}


const makeKing = cell => {
    if (whoseTurn === 'w' && cell.row === boardSize - 1) {
        cell.checker.type = checkerType.whiteKing
        becomeKing = true
    } else if (whoseTurn === 'b' && cell.row === 0) {
        cell.checker.type = checkerType.blackKing
        becomeKing = true
    }
}


const cellOnClick = (row, col) => {
    let changedCells
    let targetCell = board[row][col]

    if (inPromptMode === null || (moveList.length === 0 && inPromptMode === targetCell))
        changedCells = togglePromptMode(targetCell)

    else if (targetCell.state === cellState.canBeFilled) {
        moveList = [inPromptMode]
        changedCells = togglePromptMode(inPromptMode)
        move(moveList[0].row, moveList[0].col, row, col)
        moveList[1] = targetCell

        makeKing(targetCell)
    } else if (targetCell.state === cellState.mustBeFilled) {
        const wasInPromptMode = inPromptMode
        changedCells = togglePromptMode(inPromptMode)
        move(wasInPromptMode.row, wasInPromptMode.col, row, col)

        if (moveList.length === 0)
            moveList = [wasInPromptMode]
        moveList.push(board[row][col])

        makeKing(targetCell)

        const killedCell = situation.get(board[wasInPromptMode.row][wasInPromptMode.col]).filter(dest => dest.dest.row === row && dest.dest.col === col)[0].foe
        killedCell.state = cellState.killed
        changedCells.push(killedCell)
        killed.push(killedCell)

        calculateSituation()
        changedCells = changedCells.concat(togglePromptMode(targetCell))
    }

    buttonsVisible = (moveList.length !== 0)

    changedCells?.forEach(cell => renderCell(cell.row, cell.col))
    renderButtons()
}


const countCheckers = () => {
    whiteCounter = 0
    blackCounter = 0

    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++) {
            if (isWhite(row, col))
                whiteCounter++

            else if (isBlack(row, col))
                blackCounter++
        }
}


const resetEverything = () => {
    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            if (isPlayCell(row, col))
                board[row][col].state = cellState.default

    situation.clear()
    inPromptMode = null
    moveList = []
    becomeKing = false
    killed = []
    whoseTurn = 'w'
    buttonsVisible = false
}


const renderEverything = () => {
    renderBoard()
    renderStatus()
    renderButtons()
    moveListView.innerHTML = ''
}


const arrangementOnClick = arrangement => {
    resetEverything()
    arrangement()
    countCheckers()
    calculateSituation()
    renderEverything()
}


const cancelOnClick = () => {
    if (moveList.length === 0 && inPromptMode === null)
        return

    const curCell = moveList.length === 0 ? inPromptMode : moveList[moveList.length - 1]

    situation.get(curCell)?.forEach(dest => {
        dest.dest.state = cellState.default
        renderCell(dest.dest.row, dest.dest.col)
    })

    if (moveList.length !== 0) {
        if (becomeKing) {
            curCell.checker.type = whoseTurn === 'w' ? checkerType.white : checkerType.black
            becomeKing = false
        }

        moveList[0].checker = curCell.checker
        curCell.checker = null
        curCell.state = cellState.default

        renderCell(moveList[0].row, moveList[0].col)
        renderCell(curCell.row, curCell.col)

        moveList = []
    } else {
        inPromptMode.state = cellState.default
        renderCell(inPromptMode.row, inPromptMode.col)
    }

    inPromptMode = null

    if (killed.length !== 0) {
        let cell
        for (cell of killed) {
            const {row, col} = cell
            board[row][col].state = cellState.default
            renderCell(row, col)
        }

        killed = []
        calculateSituation()
    }

    buttonsVisible = false
    renderButtons()
}


const finishOnClick = () => {
    if (moveList.length === 0 || inPromptMode !== null)
        return

    renderMoveList()
    moveList = []
    becomeKing = false

    let cell
    for (cell of killed) {
        const {row, col} = cell
        clearChecker(row, col)
        board[row][col].state = cellState.default
        renderCell(row, col)
    }

    if (whoseTurn === 'w')
        blackCounter -= killed.length
    else
        whiteCounter -= killed.length

    killed = []

    toggleTurn()
    renderStatus()

    buttonsVisible = false
    renderButtons()
}


const moveListViewOnCopy = event => {
    event.preventDefault()
    event.clipboardData.setData('text', document.getSelection().toString().split('\n').map((line, index) => (index + 1) + '. ' + line).join('\n'))
}


const init = () => {
    renderButtons()


    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            if (isPlayCell(row, col))
                board[row][col] = {row: row, col: col, state: cellState.default}

    let col = 0

    boardView = Array.from(document.querySelectorAll('.board tr td'))
        .reduce((arr, cell, index) => {
            const row = boardSize - 1 - Math.floor(index / boardSize)

            arr[row] = arr[row] || []
            arr[row].push(isPlayCell(row, col) ? cell : null)

            col = (++col) % boardSize

            return arr
        }, Array(boardSize))

    for (let row = 0; row < boardSize; row++)
        for (let col = 0; col < boardSize; col++)
            boardView[row][col]?.addEventListener('click', () => cellOnClick(row, col))

    start.addEventListener('click', () => arrangementOnClick(startArrangement))
    example1.addEventListener('click', () => arrangementOnClick(example1Arrangement))
    cancel.addEventListener('click', () => cancelOnClick())
    finish.addEventListener('click', () => finishOnClick())
    moveListView.addEventListener('copy', event => moveListViewOnCopy(event))
}


init()