package omgu.imit.satlerWeb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SatlerWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SatlerWebApplication.class, args);
	}

}
