package omgu.imit.satlerWeb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FrontController {


    @GetMapping("/")
    public String home() {
        return index();
    }

    @GetMapping("/index")
    public String index() {
        return "index";
    }

    @GetMapping("/rules")
    public String rules() {
        return "rules";
    }

    @GetMapping("/play")
    public String play() {
        return "play";
    }


}
